use std::env;
use std::error::Error;
use std::fs::File;
use std::path::Path;
use std::io::Read;
use std::collections::HashMap;


extern crate serde;
extern crate serde_json;

#[macro_use]
extern crate serde_derive;

//
// TIPOS
//


#[derive(Serialize, Deserialize, Debug, Clone)]
struct Funcionario {
    area: String,
    id: i32,
    nome: String,
    salario: f32,
    sobrenome: String,
}


#[derive(Serialize, Deserialize)]
struct Area {
    codigo: String,
    nome: String,
}

#[derive(Serialize, Deserialize)]
struct Dump {
    funcionarios: Vec<Funcionario>,
    areas: Vec<Area>,
}

#[derive(Debug)]
struct Acumulador {
    nome: String,
    min: f32,
    max: f32,
    media: f64,
    total: i32,
    max_f: Vec<Funcionario>,
    min_f: Vec<Funcionario>,
}

//
// Helper para o nome da area
//

fn nome_area(area: String, areas: &[Area]) -> String {
    let mut into_iter = areas.iter();
    let a = into_iter.find(| ref x| x.codigo == area);

    let resposta = match a {
        Some(a) => a.nome.clone(),
        _ => area
    };

    return resposta;

    
}

//
// FUNCAO QUE CALCULA TUDO
//

fn processa_json<P: AsRef<Path>>(path: P) -> Result<(), Box<Error>> {
    // Some JSON input data as a &str. Maybe this comes from the user.
    let mut s = String::new();
    File::open(path).unwrap().read_to_string(&mut s).unwrap();

    // Parse the string of data into serde_json::Value.
    let json: Dump = serde_json::from_str(&s).unwrap();

    // Access parts of the data by indexing with square brackets.

    let mut max: f32 = 0.0;
    let mut media: f64 = 0.0;
    let total = json.funcionarios.len() as i32;
    let mut min: f32 = 999999.0;
    let mut max_f: Vec<Funcionario> = vec![];
    let mut min_f: Vec<Funcionario> = vec![];
    let mut areas: HashMap<&str, Acumulador> = HashMap::new();
    let mut sobrenomes: HashMap<&str, Acumulador> = HashMap::new();

    for f in json.funcionarios.iter() {
        media += f.salario as f64;


        if f.salario == max {
            max = f.salario;
            max_f.push(f.clone());
        }

        if f.salario > max {
            max = f.salario;
            max_f = vec![f.clone()];
        }

       
        if f.salario == min {
            min = f.salario;
            min_f.push(f.clone());
        }

        if f.salario < min {
            min = f.salario;
            min_f = vec![f.clone()];
        }


        let area = areas.entry(&f.area).or_insert(Acumulador {nome: f.area.clone(), min: 9999.0, max: 0.0, total: 0 , media: 0.0, min_f: vec![], max_f: vec![]});

        if f.area == area.nome {
            area.total = area.total + 1;
            area.media = area.media + f.salario as f64;

           

            if f.salario == area.max {
                area.max = f.salario;
                area.max_f.push(f.clone());
            }

            if f.salario > area.max {
                area.max = f.salario;
                area.max_f = vec![f.clone()];
            }

            if f.salario == area.min {
                area.min = f.salario;
                area.min_f.push(f.clone());
            }

            if f.salario < area.min {
                area.min = f.salario;
                area.min_f = vec![f.clone()];
            }

           
        }

        let sobrenome = sobrenomes.entry(&f.sobrenome).or_insert(Acumulador {nome: f.sobrenome.clone(), min: 9999.0, max: 0.0, total: 0 , media: 0.0, min_f: vec![], max_f: vec![]});

        if f.sobrenome == sobrenome.nome {
            sobrenome.total = sobrenome.total + 1;
            sobrenome.media = sobrenome.media + f.salario as f64;

            if f.salario == sobrenome.max {
                sobrenome.max = f.salario;
                sobrenome.max_f.push(f.clone());
            }

            if f.salario > sobrenome.max {
                sobrenome.max = f.salario;
                sobrenome.max_f = vec![f.clone()];
            }

            if f.salario == sobrenome.min {
                sobrenome.min = f.salario;
                sobrenome.min_f.push(f.clone());
            }

            if f.salario < sobrenome.min {
                sobrenome.min = f.salario;
                sobrenome.min_f = vec![f.clone()];
            }

           
        }

    }

    media = media / total as f64;


    //
    // IMPRIME RELATORIO
    //

    println!("global_avg|{:.2}", media);

    for f in max_f.iter() {
        println!("global_max|{} {}|{:.2}", f.nome, f.sobrenome, f.salario);
    }

    for f in min_f.iter() {
        println!("global_min|{} {}|{:.2}", f.nome, f.sobrenome, f.salario);
    }


    let mut max: i32 = 0;
    let mut max_a: Vec<String> = vec![];
    let mut min: i32 = 9999999;
    let mut min_a: Vec<String> = vec![];

    for (_nome, f) in areas.iter() {
        if f.total == max {
            max_a.push(f.nome.clone());
            max = f.total;
        }

        if f.total > max {
            max_a = vec![f.nome.clone()];
            max = f.total;
        }

        if f.total == min {
            min_a.push(f.nome.clone());
            min = f.total;
        }

        if f.total < min {
            min_a = vec![f.nome.clone()];
            min = f.total;
        }

        println!("area_avg|{}|{:.2}", nome_area(f.nome.clone(), &json.areas), f.media / f.total as f64);

        for a in &f.max_f {
            println!("area_max|{}|{} {}|{:.2}", nome_area(f.nome.clone(), &json.areas),  a.nome, a.sobrenome, a.salario);
        }

        for a in &f.min_f {
            println!("area_min|{}|{} {}|{:.2}", nome_area(f.nome.clone(), &json.areas),  a.nome, a.sobrenome, a.salario);
        }

    }

    for a in max_a {
        println!("most_employees|{}|{}", nome_area(a, &json.areas), max);
    }

    for a in min_a {
        println!("least_employees|{}|{}", nome_area(a, &json.areas), min);
    }

    


    for (_nome, a) in sobrenomes.iter() {
        if a.total > 1 {
            for f in a.max_f.iter() {
                println!("last_name_max|{}|{} {}|{:.2}", f.sobrenome, f.nome, f.sobrenome, f.salario);
            }
        }
    }

    Ok(())
}

//
// INICIA O PROGRAMA
//

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];

    let _res = processa_json(filename);
}
