# README #

Repositorio do Desafio 5

### Como rodar ###

* Instale o [Rust](http://rust-lang.org).
* Na pasta do repositorio use:
	* ```cargo build --release```
	* Isso ira criar ```/targer/release/hello``` que eh o app.
* Rode com:
	* ```/target/release/hello ./funcionarios-3M.json```